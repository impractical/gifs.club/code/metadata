package storers

import (
	"context"

	"gifs.club/code/metadata"

	memdb "github.com/hashicorp/go-memdb"
	"github.com/pkg/errors"
)

var (
	schema = &memdb.DBSchema{
		Tables: map[string]*memdb.TableSchema{
			"collection": &memdb.TableSchema{
				Name: "collection",
				Indexes: map[string]*memdb.IndexSchema{
					"id": &memdb.IndexSchema{
						Name:   "id",
						Unique: true,
						Indexer: &memdb.StringFieldIndex{
							Field:     "ID",
							Lowercase: true,
						},
					},
					"slug": &memdb.IndexSchema{
						Name:   "slug",
						Unique: true,
						Indexer: &memdb.StringFieldIndex{
							Field:     "Slug",
							Lowercase: true,
						},
					},
				},
			},
			"file": &memdb.TableSchema{
				Name: "file",
				Indexes: map[string]*memdb.IndexSchema{
					"id": &memdb.IndexSchema{
						Name:   "id",
						Unique: true,
						Indexer: &memdb.CompoundIndex{
							AllowMissing: false,
							Indexes: []memdb.Indexer{
								&memdb.StringFieldIndex{
									Field: "Hash",
								},
								&memdb.StringFieldIndex{
									Field: "Extension",
								},
							},
						},
					},
				},
			},
			"gif": &memdb.TableSchema{
				Name: "gif",
				Indexes: map[string]*memdb.IndexSchema{
					"id": &memdb.IndexSchema{
						Name:   "id",
						Unique: true,
						Indexer: &memdb.StringFieldIndex{
							Field:     "ID",
							Lowercase: true,
						},
					},
					"url": &memdb.IndexSchema{
						Name:   "url",
						Unique: true,
						Indexer: &memdb.CompoundIndex{
							AllowMissing: false,
							Indexes: []memdb.Indexer{
								&memdb.StringFieldIndex{
									Field:     "Collection",
									Lowercase: true,
								},
								&memdb.StringFieldIndex{
									Field: "Path",
								},
							},
						},
					},
				},
			},
		},
	}
)

type Memory struct {
	db *memdb.MemDB
}

func NewMemory() (*Memory, error) {
	db, err := memdb.NewMemDB(schema)
	if err != nil {
		return nil, err
	}
	return &Memory{
		db: db,
	}, nil
}

func (m *Memory) CreateCollection(ctx context.Context, coll metadata.Collection) error {
	txn := m.db.Txn(true)
	defer txn.Abort()
	exists, err := txn.First("collection", "id", coll.ID)
	if err != nil {
		return errors.Wrap(err, "error checking if collection ID exists")
	}
	if exists != nil {
		return metadata.ErrCollectionAlreadyExists
	}
	exists, err = txn.First("collection", "slug", coll.Slug)
	if err != nil {
		return errors.Wrap(err, "error checking if collection slug exists")
	}
	if exists != nil {
		return metadata.ErrCollectionSlugAlreadyExists
	}
	err = txn.Insert("collection", &coll)
	if err != nil {
		return errors.Wrap(err, "error inserting collection")
	}
	txn.Commit()
	return nil
}

func (m *Memory) UpdateCollection(ctx context.Context, id string, change metadata.CollectionChange) error {
	txn := m.db.Txn(true)
	defer txn.Abort()
	coll, err := txn.First("collection", "id", id)
	if err != nil {
		return errors.Wrap(err, "error retrieving collection")
	}
	if coll == nil {
		return metadata.ErrCollectionNotFound
	}
	updated := coll.(*metadata.Collection).Apply(change)
	err = txn.Insert("collection", &updated)
	if err != nil {
		return errors.Wrap(err, "error inserting updated collection")
	}
	txn.Commit()
	return nil
}

func (m *Memory) DeleteCollection(ctx context.Context, id string) error {
	txn := m.db.Txn(true)
	defer txn.Abort()
	exists, err := txn.First("collection", "id", id)
	if err != nil {
		return errors.Wrap(err, "error checking if collection exists")
	}
	if exists == nil {
		return metadata.ErrCollectionNotFound
	}
	err = txn.Delete("collection", exists)
	if err != nil {
		return errors.Wrap(err, "error deleting collection")
	}
	txn.Commit()
	return nil
}

func (m *Memory) GetCollection(ctx context.Context, id string) (metadata.Collection, error) {
	txn := m.db.Txn(false)
	coll, err := txn.First("collection", "id", id)
	if err != nil {
		return metadata.Collection{}, errors.Wrap(err, "error retrieving collection")
	}
	if coll == nil {
		return metadata.Collection{}, metadata.ErrCollectionNotFound
	}
	return *coll.(*metadata.Collection), nil
}

func (m *Memory) GetCollectionBySlug(ctx context.Context, slug string) (metadata.Collection, error) {
	txn := m.db.Txn(false)
	coll, err := txn.First("collection", "slug", slug)
	if err != nil {
		return metadata.Collection{}, errors.Wrap(err, "error retrieving collection")
	}
	if coll == nil {
		return metadata.Collection{}, metadata.ErrCollectionNotFound
	}
	return *coll.(*metadata.Collection), nil
}

func (m *Memory) ListCollections(ctx context.Context, filter metadata.CollectionFilter) ([]metadata.Collection, error) {
	txn := m.db.Txn(false)
	var colls []metadata.Collection
	collIter, err := txn.Get("collection", "id")
	if err != nil {
		return nil, errors.Wrap(err, "error listing collections")
	}
	for {
		coll := collIter.Next()
		if coll == nil {
			break
		}
		// TODO(paddy): apply filtering to collection listing
		colls = append(colls, *coll.(*metadata.Collection))
	}
	metadata.CollectionsByLastUploadAt(colls)
	return colls, nil
}

func (m *Memory) CreateGIF(ctx context.Context, gif metadata.GIF) error {
	txn := m.db.Txn(true)
	defer txn.Abort()
	exists, err := txn.First("gif", "id", gif.ID)
	if err != nil {
		return errors.Wrap(err, "error checking if GIF ID exists")
	}
	if exists != nil {
		return metadata.ErrGIFAlreadyExists
	}
	exists, err = txn.First("gif", "url", gif.Collection, gif.Path)
	if err != nil {
		return errors.Wrap(err, "error checking if GIF path exists")
	}
	if exists != nil {
		return metadata.ErrGIFPathAlreadyExists
	}
	err = txn.Insert("gif", &gif)
	if err != nil {
		return errors.Wrap(err, "error inserting GIF")
	}
	txn.Commit()
	return nil
}

func (m *Memory) UpdateGIF(ctx context.Context, id string, change metadata.GIFChange) error {
	return nil
}

func (m *Memory) DeleteGIF(ctx context.Context, id string) error {
	return nil
}

func (m *Memory) GetGIF(ctx context.Context, id string) (metadata.GIF, error) {
	return metadata.GIF{}, nil
}

func (m *Memory) ListGIFs(ctx context.Context, filter metadata.GIFFilter) ([]metadata.GIF, error) {
	return nil, nil
}

func (m *Memory) CreateFile(ctx context.Context, file metadata.File) error {
	txn := m.db.Txn(true)
	defer txn.Abort()
	exists, err := txn.First("file", "id", file.Hash, file.Extension)
	if err != nil {
		return errors.Wrap(err, "error checking if file hash exists")
	}
	if exists != nil {
		return metadata.ErrFileAlreadyExists
	}
	err = txn.Insert("file", &file)
	if err != nil {
		return errors.Wrap(err, "error inserting file")
	}
	txn.Commit()
	return nil
}

func (m *Memory) UpdateFile(ctx context.Context, id string, change metadata.FileChange) error {
	return nil
}

func (m *Memory) DeleteFile(ctx context.Context, id string) error {
	return nil
}

func (m *Memory) GetFile(ctx context.Context, id string) (metadata.File, error) {
	return metadata.File{}, nil
}

func (m *Memory) ListFiles(ctx context.Context, filters metadata.FileFilter) ([]metadata.File, error) {
	return nil, nil
}

func (m *Memory) VacuumFiles(ctx context.Context) ([]string, error) {
	return nil, nil
}
