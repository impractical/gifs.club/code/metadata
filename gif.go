package metadata

import (
	"time"

	"github.com/pkg/errors"
)

var (
	ErrGIFAlreadyExists     = errors.New("gif already exists")
	ErrGIFPathAlreadyExists = errors.New("gif path already exists")
)

type GIF struct {
	ID         string
	Collection string
	Path       string
	FileHash   string
	CreatedAt  time.Time
	CreatedBy  string
}

func (g GIF) GetSQLTableName() string {
	return "gifs"
}

func (g GIF) Apply(change GIFChange) GIF {
	return g
}

type GIFChange struct{}

type GIFFilter struct{}
