package metadata

import "context"

//go:generate go-bindata -pkg migrations -o migrations/generated.go sql/

type GIFStorer interface {
	CreateGIF(context.Context, GIF) error
	UpdateGIF(context.Context, string, GIFChange) error
	DeleteGIF(context.Context, string) error
	GetGIF(context.Context, string) (GIF, error)
	ListGIFs(context.Context, GIFFilter) ([]GIF, error)

	CreateFile(context.Context, File) error
	UpdateFile(context.Context, string, FileChange) error
	DeleteFile(context.Context, string) error
	GetFile(context.Context, string) (File, error)
	ListFiles(context.Context, FileFilter) ([]File, error)
	VacuumFiles(context.Context) ([]string, error)
}

type CollectionStorer interface {
	CreateCollection(context.Context, Collection) error
	UpdateCollection(context.Context, string, CollectionChange) error
	DeleteCollection(context.Context, string) error
	GetCollection(context.Context, string) (Collection, error)
	GetCollectionBySlug(context.Context, string) (Collection, error)
	ListCollections(context.Context, CollectionFilter) ([]Collection, error)
}
