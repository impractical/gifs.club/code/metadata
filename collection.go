package metadata

import (
	"sort"
	"time"

	"github.com/pkg/errors"
)

var (
	ErrCollectionAlreadyExists     = errors.New("collection with that ID already exists")
	ErrCollectionNotFound          = errors.New("collection not found")
	ErrCollectionSlugAlreadyExists = errors.New("collection with that Slug already exists")
)

type Collection struct {
	ID           string
	Slug         string
	Name         string
	Description  string
	CreatedAt    time.Time
	LastUploadAt time.Time
	Owners       []string
}

func (c Collection) GetSQLTableName() string {
	return "collections"
}

func (c Collection) Apply(change CollectionChange) Collection {
	return c
}

type CollectionChange struct {
	Slug         *string
	Name         *string
	Description  *string
	LastUploadAt time.Time
	Owners       []string
}

type CollectionFilter struct {
	IDs                []string
	Slugs              []string
	LastUploadAtAfter  *time.Time
	LastUploadAtBefore *time.Time
	Owners             []string
}

func CollectionsByLastUploadAt(c []Collection) {
	sort.Slice(c, func(i, j int) bool { return c[i].LastUploadAt.After(c[j].LastUploadAt) })
}
