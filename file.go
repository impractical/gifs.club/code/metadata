package metadata

import (
	"time"

	"github.com/pkg/errors"
)

var (
	ErrFileAlreadyExists = errors.New("file already exists")
)

type File struct {
	Hash             string
	Extension        string
	ContentType      string
	FileSize         int64
	PlaceholderColor string
	PlaceholderBytes []byte
	CreatedAt        time.Time
	FirstUploadedBy  string
}

func (f File) GetSQLTableName() string {
	return "files"
}

func (f File) Apply(change FileChange) File {
	return f
}

type FileChange struct {
	PlaceholderColor *string
	PlaceholderBytes []byte
}

type FileFilter struct {
	FileSizeGreaterThan int64
	FileSizeLessThan    int64
	Hashes              []string
	ContentTypes        []string
	Extensions          []string
}
